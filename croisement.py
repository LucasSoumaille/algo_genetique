def croisement(list_cities):

    return_list = []
    inner_list = []
    temp_list = []
    parent_list = []
    first_it = []

    for x in range(len(list_cities)):

        mid_size_inner_list = int((len(list_cities[x])/2)-1)
        lenght_inner_list = int(len(list_cities[x])-1)
        parent_list.clear()

        if x % 2 == 0:
            inner_list.clear()
            temp_list.clear()

        for y in range(len(list_cities[x])):
            city_dont_exist = False

            if x == 0 and 0 <= y < lenght_inner_list:
                first_it.append(list_cities[x][y])

            if 0 <= y < lenght_inner_list:
                parent_list.append(list_cities[x][y])

            # Si on est dans la dernière liste et quelle est pair
            if x == len(list_cities) and x % 2 == 0:
                inner_list.append(list_cities[x][y])

            # Si on est dans la première partie de la première liste
            elif y < mid_size_inner_list and x % 2 == 0:

                # On ajoute les villes dans la nouvelle liste
                inner_list.append(list_cities[x][y])

            # Si on est dans la première partie de la seconde liste
            elif y < mid_size_inner_list and x % 2 == 1:

                # On sauvegarde les valeurs dans une variable temporaire
                temp_list.append(list_cities[x][y])

            # Sinon si on est dans la seconde partie de la deuxième liste
            elif mid_size_inner_list <= y < lenght_inner_list and x % 2 == 1:

                # On regarde toutes les villes dans la nouvelle liste
                replacement_city = ""

                for city in inner_list:

                    # Si la ville qu'on veut ajouter n'existe pas
                    if city != list_cities[x][y]:

                        city_dont_exist = True

                    elif city == list_cities[x][y]:

                        city_dont_exist = False
                        city_to_replace = list_cities[x][y]

                        # Sinon on parcours la liste temp pour trouver la première ville dispo et l'ajouter
                        for city_temp in temp_list:

                            if city_to_replace != city_temp and not (city_temp in inner_list):
                                replacement_city = city_temp
                                break
                        break

                # On l'ajoute
                if city_dont_exist:
                    inner_list.append(list_cities[x][y])
                elif not city_dont_exist:
                    inner_list.append(replacement_city)

        return_list.append(parent_list[:])

        if x == 0:
            return_list.append(first_it)
        if inner_list in return_list:
            print("--> l'enfant existe déjà en tant que parent <--")
        elif x % 2 == 1 and not (inner_list in list_cities) or x == len(list_cities) and x % 2 == 0 and\
                not (inner_list in list_cities):
            return_list.append(inner_list[:])

    return return_list


__all__ = [
    "croisement"
]
