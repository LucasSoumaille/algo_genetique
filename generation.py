import random

list_cities = []


# Méthode de génération de l'échantillon de base
def generation(data, lengh_sample):

    # Tant que la liste n'a pas atteint la longueur voulu, on génère des listes de villes aléatoire
    while len(list_cities) < lengh_sample:
        random.shuffle(data)
        city = data

        # Si la liste contient le même enregistrement, on ne l'ajoute pas
        if city[:] in list_cities:
            print("-- > un échantillon similaire a été généré, il n'est pas sauvegardé <--")
        else:
            list_cities.append(city[:])

    print(len(list_cities), "/", lengh_sample)

    # On affiche sur la sortie standard la confirmation
    print("--> échantillon de base généré avec succès <--")

    return list_cities


def second_generation(data, list_cities_2, lengh_sample):

    # Tant que la liste n'a pas atteint la longueur voulu, on génère des listes de villes aléatoire
    while len(list_cities_2) < lengh_sample:
        random.shuffle(data)
        city = data

        # Si la liste contient le même enregistrement, on ne l'ajoute pas
        if city[:] in list_cities_2:
            print("-- > un échantillon similaire a été généré, il n'est pas sauvegardé <--")
        else:
            list_cities_2.append(city[:])

    # On affiche sur la sortie standard la confirmation
    print("--> échantillon complété avec succès <--")

    return list_cities_2


__all__ = [
    "generation",
    "second_generation"
]
