def test_number_city(list_cities, goal):

    bordeaux = 0
    pau = 0
    nantes = 0
    paris = 0
    lille = 0
    marseille = 0
    nice = 0
    lyon = 0
    montpellier = 0
    toulouse = 0
    brest = 0
    limoges = 0
    clermont = 0
    tours = 0
    strasbourg = 0

    for iteration in list_cities:
        for city in iteration:

            if 'Bordeaux' in city.values():
                bordeaux += 1
            if "Pau" in city.values():
                pau += 1
            if "Nantes" in city.values():
                nantes += 1
            if "Paris" in city.values():
                paris += 1
            if "Lille" in city.values():
                lille += 1
            if "Marseille" in city.values():
                marseille += 1
            if "Nice" in city.values():
                nice += 1
            if "Lyon" in city.values():
                lyon += 1
            if "Montpellier" in city.values():
                montpellier += 1
            if "Toulouse" in city.values():
                toulouse += 1
            if "Brest" in city.values():
                brest += 1
            if "Limoges" in city.values():
                limoges += 1
            if "Clermont-Ferrand" in city.values():
                clermont += 1
            if "Tours" in city.values():
                tours += 1
            if "Strasbourg" in city.values():
                strasbourg += 1

    if goal == bordeaux == pau == nantes == paris == lille == marseille == nice\
            == lyon == montpellier == toulouse == brest == limoges == clermont == tours == strasbourg:
        print("--> Le nb d'itération > ", goal, " < de chaque ville est respécté <--")
    else:
        print("--> Le nb d'itération > ", goal, " < de chaque ville n'est pas respécté <--")
