def select_sort(list_cities):

    # Tri par distance
    sorted_list = sorted(list_cities, key=lambda k: k[-1]["distance"])

    print("--> liste triée avec succès <--")

    # Calcul des pourcentages des échantillons à prendre
    fifty_percent = int((len(list_cities)/4)*0.5)
    thirty_percent = int((len(list_cities)/4)*0.3)
    fifteen_percent = int((len(list_cities)/4)*0.15)
    five_percent = int((len(list_cities)/4)*0.05)

    # Délimitation du début des quartiles
    beg_f_quarter = 0
    beg_s_quarter = int(len(list_cities)/4)
    beg_t_quarter = int(len(list_cities)/2)
    beg_l_quarter = int(len(list_cities)/2 + len(list_cities)/4)

    new_list = []

    # Sélection d'~ 1/4 de la liste parmis les 4 quartiles
    for x in range(len(sorted_list)):
        if beg_f_quarter <= x < fifty_percent:
            new_list.append(sorted_list[x])

        elif beg_s_quarter <= x < beg_s_quarter + thirty_percent:
            new_list.append(sorted_list[x])

        elif beg_t_quarter <= x < beg_t_quarter + fifteen_percent:
            new_list.append(sorted_list[x])

        elif beg_l_quarter <= x < beg_l_quarter + five_percent:
            new_list.append(sorted_list[x])

    print("--> selection effectuée avec succès <--")
    return new_list


def sort(list_cities2):

    sorted_list = sorted(list_cities2, key=lambda k: k[-1]["distance"])
    print("--> liste triée avec succès <--")
    return sorted_list


__all__ = [
    "select_sort",
    "sort"
]
