from math import sin, cos, sqrt, atan2, radians

# Rayon aproximatif de la terre en km
R = 6373.0


# Méthode de calcul des distances
def calcul_distance(list_cities):
    i = 0

    # Parcours de la liste principal et instanciation
    for iteration in list_cities:
        lat2 = 0
        long2 = 0
        distance = 0

        # Parcours des listes secondaires
        for cities in iteration:
            lat1 = radians(cities['lan'])
            long1 = radians(cities['lng'])

            # Si premier enregistrement, on ne calcul pas
            if lat2 != 0 and long2 != 0:

                # Calcul des distances
                dlon = long2 - long1
                dlat = lat2 - lat1

                a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
                c = 2 * atan2(sqrt(a), sqrt(1 - a))
                distance_temp = R * c

                # Ajout des distances entre chaque ville pour une liste
                distance += distance_temp

            lat2 = radians(cities['lan'])
            long2 = radians(cities['lng'])

        string_dist = {'distance': distance}

        # Ajout de la valeur distance à la fin de la liste (pour chaque liste)
        list_cities[i].append(string_dist)
        i += 1

    print("--> calcul des distances effectué avec succès <--")

    return list_cities


__all__ = [
    "calcul_distance"
]
