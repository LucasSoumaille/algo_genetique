from random import randint


def mutation(list_cities, value, size_list_city):

    for iteration in list_cities:
        random_list = randint(1, value)
        if random_list == 1:
            random_index1 = randint(0, size_list_city-1)
            random_index2 = randint(0, size_list_city-1)
            print("--> mutation déclenchée <--")
            temp = iteration[random_index1]
            iteration[random_index1] = iteration[random_index2]
            iteration[random_index2] = temp

    return list_cities


__all__ = [
    "mutation"
]
