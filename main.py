import json
from generation import *
from distance import *
from selection import *
from croisement import *
from mutation import *
from betterdistance import *

# Indique la longueur de la liste à générer
length_sample = 200
# Indique la chance que la mutation se déclenche (200 pour 1/200 par ex)
mutation_base = 10000
# Indique le nombre d'itérations du processus
iteration = 2000


def reader():

    with open('cities.json') as json_file:
        data = json.load(json_file)
        return data


def main():

    print("### Début du processus ###")

    after_mutation = []
    list_iteration = []
    list_min = []
    i = 0

    data = reader()
    size_list_city = len(data)
    basic_sample = generation(data, length_sample)

    for x in range(iteration):
        print("### Répétition, étape ", x+1, "/", iteration, "  ###")
        if x != 0:
            basic_sample = second_generation(data, after_mutation, length_sample)

        list_with_distance = calcul_distance(basic_sample)
        selected_list = select_sort(list_with_distance)
        mininimum_dist = get_minimum(list_with_distance)
        custom = croisement(selected_list)
        after_mutation = mutation(custom, mutation_base, size_list_city)
        if x < 1:
            list_iteration.append(i)
            list_min.append(mininimum_dist)
            i += 1
        elif mininimum_dist <= list_min[-1]:
            list_iteration.append(i)
            list_min.append(mininimum_dist)
            i += 1

    list_with_distance2 = calcul_distance(after_mutation)
    sort_list = sort(list_with_distance2)
    mininimum_dist = get_minimum(sort_list)
    list_iteration.append(i)
    list_min.append(mininimum_dist)
    do_graph(list_min, list_iteration)
    print("### Fin du processus ###")
    get_better_distance(sort_list)


if __name__ == '__main__':
    main()
