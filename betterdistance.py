import matplotlib.pyplot as plt


def get_better_distance(list_city):

    response = "Le trajet optimal trouvé est :"

    first = next(iter(list_city), None)
    for values in first:
        if "city" in values:
            city = values["city"]
            response += " - " + city
        if "distance" in values:
            mininimum_dist = values["distance"]
            response += ", pour un total de : " + str(mininimum_dist)

    print("--> ", response)

    return first


def get_minimum(list_cities):

    mininimum_dist = 0

    for x in range(len(list_cities)):
        if x == 0:
            for y in list_cities[x]:
                if "distance" in y:
                    mininimum_dist = y["distance"]

    return mininimum_dist


def do_graph(list_moyenne_distance, list_iter):

    plt.plot(list_iter, list_moyenne_distance)

    plt.xlabel('itération')
    plt.ylabel('distance')

    plt.title("Évolution des distances optimales")

    plt.show()


__all__ = [
    "get_better_distance",
    "get_minimum",
    "do_graph"
]
